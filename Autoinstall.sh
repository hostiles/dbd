#!/bin/bash

# Update and upgrade system packages
sudo apt-get update -y
sudo apt-get upgrade -y

# Install required dependencies
sudo apt-get install -y git make gcc

# Clone the repository
git clone https://gitlab.com/kalilinux/packages/dbd/

# Change directory to dbd
cd dbd

# Compile the dbd program
make unix
gcc dbd.c -o dbd

# Run the dbd program
./dbd

echo "dbd installed and running."
